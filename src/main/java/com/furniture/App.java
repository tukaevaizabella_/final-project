package com.furniture;

import com.furniture.controller.ProductController;
import com.furniture.repository.ProductRepository;
import com.furniture.repository.TextFileProductRepository;
import com.furniture.service.ProductService;
import com.furniture.view.ConsoleView;

public class App {
    public static void main(String[] args) {
        ProductRepository repository = new TextFileProductRepository();
        ProductService service = new ProductService(repository);
        ConsoleView view = new ConsoleView(service);
        ProductController controller = new ProductController(view);
        controller.startApplication();
    }
}
