package com.furniture.controller;

import com.furniture.view.ConsoleView;

public class ProductController {
    private final ConsoleView consoleView;

    public ProductController(ConsoleView consoleView) {
        this.consoleView = consoleView;
    }

    public void startApplication() {
        displayApplicationInformation();
        consoleView.run();
    }

    void displayApplicationInformation() {
        System.out.println("=== Furniture Warehouse Search System ===");
        System.out.println("Version: 1.0");
        System.out.println("Developer: Izabella");
        System.out.println("Email: Izabella_Tukaeva@student.itpu.uz");
        System.out.println();
    }


}
