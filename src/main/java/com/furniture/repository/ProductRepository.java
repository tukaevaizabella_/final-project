package com.furniture.repository;

import com.furniture.model.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> getAllProducts();
    List<Product> searchProductsByName(String name);
    List<Product> searchProductsByCategory(String category);
    List<Product> searchProductsByPriceRange(double minPrice, double maxPrice);
    List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity);


}
