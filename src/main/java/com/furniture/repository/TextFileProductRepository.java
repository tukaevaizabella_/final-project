package com.furniture.repository;

import com.furniture.model.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextFileProductRepository implements ProductRepository {
    private static final String DATA_FILE_PATH = "src/resources/data.txt";

    public TextFileProductRepository() {
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(DATA_FILE_PATH))) {
            String line;
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                int id = Integer.parseInt(data[0]);
                String name = data[1];
                String category = data[2];
                double price = Double.parseDouble(data[3]);
                int quantity = Integer.parseInt(data[4]);

                Product product = new Product(id, name, category, price, quantity);
                products.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return products;
    }

    @Override
    public List<Product> searchProductsByName(String name) {
        List<Product> products = getAllProducts();
        List<Product> matchingProducts = new ArrayList<>();

        for (Product product : products) {
            if (product.getName().equalsIgnoreCase(name)) {
                matchingProducts.add(product);
            }
        }

        return matchingProducts;
    }

    @Override
    public List<Product> searchProductsByCategory(String category) {
        List<Product> products = getAllProducts();
        List<Product> matchingProducts = new ArrayList<>();

        for (Product product : products) {
            if (product.getCategory().equalsIgnoreCase(category)) {
                matchingProducts.add(product);
            }
        }

        return matchingProducts;
    }

    @Override
    public List<Product> searchProductsByPriceRange(double minPrice, double maxPrice) {
        List<Product> products = getAllProducts();
        List<Product> matchingProducts = new ArrayList<>();

        for (Product product : products) {
            if (product.getPrice() >= minPrice && product.getPrice() <= maxPrice) {
                matchingProducts.add(product);
            }
        }

        return matchingProducts;
    }

    @Override
    public List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity) {
        List<Product> products = getAllProducts();
        List<Product> matchingProducts = new ArrayList<>();

        for (Product product : products) {
            if (product.getQuantity() >= minQuantity && product.getQuantity() <= maxQuantity) {
                matchingProducts.add(product);
            }
        }

        return matchingProducts;
    }
}