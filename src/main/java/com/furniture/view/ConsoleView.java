package com.furniture.view;


import com.furniture.model.Product;
import com.furniture.service.ProductService;

import java.util.List;
import java.util.Scanner;

public class ConsoleView {
    private final ProductService productService;
    private final Scanner scanner;

    public ConsoleView(ProductService productService) {
        this.productService = productService;
        this.scanner = new Scanner(System.in);
    }

    public void displayMenu() {
        System.out.println("=== Warehouse Search System ===");
        System.out.println("1. Search products by name");
        System.out.println("2. Search products by category");
        System.out.println("3. Search products by price range");
        System.out.println("4. Search products by quantity range");
        System.out.println("5. List all products");
        System.out.println("6. Exit");
    }

    public void run() {
        System.out.println("Welcome to the Warehouse Search System!");

        while (true) {
            displayMenu();
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1 -> searchProductsByName();
                case 2 -> searchProductsByCategory();
                case 3 -> searchProductsByPriceRange();
                case 4 -> searchProductsByQuantityRange();
                case 5 -> listAllProducts();
                case 6 -> {
                    System.out.println("Thank you for using the Warehouse Search System. Goodbye!");
                    return;
                }
                default -> System.out.println("Invalid choice. Please try again.");
            }

            System.out.println();
        }
    }

    private void searchProductsByName() {
        System.out.print("Enter the product name: ");
        String name = scanner.nextLine();

        List<Product> products = productService.searchProductsByName(name);

        if (products.isEmpty()) {
            System.out.println("No products found with the name: " + name);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByCategory() {
        System.out.print("Enter the product category: ");
        String category = scanner.nextLine();

        List<Product> products = productService.searchProductsByCategory(category);

        if (products.isEmpty()) {
            System.out.println("No products found in the category: " + category);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByPriceRange() {
        System.out.print("Enter the minimum price: ");
        double minPrice = scanner.nextDouble();
        System.out.print("Enter the maximum price: ");
        double maxPrice = scanner.nextDouble();

        List<Product> products = productService.searchProductsByPriceRange(minPrice, maxPrice);

        if (products.isEmpty()) {
            System.out.println("No products found within the price range: " + minPrice + " - " + maxPrice);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByQuantityRange() {
        System.out.print("Enter the minimum quantity: ");
        int minQuantity = scanner.nextInt();
        System.out.print("Enter the maximum quantity: ");
        int maxQuantity = scanner.nextInt();

        List<Product> products = productService.searchProductsByQuantityRange(minQuantity, maxQuantity);

        if (products.isEmpty()) {
            System.out.println("No products found within the quantity range: " + minQuantity + " - " + maxQuantity);
        } else {
            displayProductList(products);
        }
    }

    private void listAllProducts() {
        List<Product> products = productService.getAllProducts();

        if (products.isEmpty()) {
            System.out.println("No products available in the inventory.");
        } else {
            displayProductList(products);
        }
    }

    private void displayProductList(List<Product> products) {
        System.out.println("=== Product List ===");
        for (Product product : products) {
            System.out.println(product);
        }
    }
}
