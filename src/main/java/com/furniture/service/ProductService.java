package com.furniture.service;

import com.furniture.model.Product;
import com.furniture.repository.ProductRepository;

import java.util.List;

public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public List<Product> searchProductsByName(String name) {
        return productRepository.searchProductsByName(name);
    }

    public List<Product> searchProductsByCategory(String category) {
        return productRepository.searchProductsByCategory(category);
    }

    public List<Product> searchProductsByPriceRange(double minPrice, double maxPrice) {
        return productRepository.searchProductsByPriceRange(minPrice, maxPrice);
    }

    public List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity) {
        return productRepository.searchProductsByQuantityRange(minQuantity, maxQuantity);
    }
}
