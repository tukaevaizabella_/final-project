package com.furniture.controller;

import com.furniture.view.ConsoleView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class ProductControllerTest {
    private ConsoleView consoleView;
    private ProductController productController;

    @BeforeEach
    public void setup() {
        consoleView = Mockito.mock(ConsoleView.class);
        productController = new ProductController(consoleView);
    }

    @Test
    public void testStartApplication() {
        productController.startApplication();
        verify(consoleView, times(1)).run();
    }


}
