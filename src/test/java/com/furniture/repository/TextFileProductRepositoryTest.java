package com.furniture.repository;

import com.furniture.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TextFileProductRepositoryTest {
    private TextFileProductRepository repository;

    @BeforeEach
    public void setUp() {
        repository = new TextFileProductRepository();
    }

    @Test
    public void testGetAllProducts() {
        List<Product> products = repository.getAllProducts();
        Assertions.assertFalse(products.isEmpty());
        Assertions.assertEquals(34, products.size());
    }

    @Test
    public void testSearchProductsByName() {
        String name = "Chair";
        List<Product> matchingProducts = repository.searchProductsByName(name);
        Assertions.assertFalse(matchingProducts.isEmpty());
        for (Product product : matchingProducts) {
            Assertions.assertEquals(name, product.getName());
        }
    }

    @Test
    public void testSearchProductsByCategory() {
        String category = "Red";
        List<Product> matchingProducts = repository.searchProductsByCategory(category);

        Assertions.assertFalse(matchingProducts.isEmpty());

        for (Product product : matchingProducts) {
            Assertions.assertEquals(category, product.getCategory());
        }
    }

    @Test
    public void testSearchProductsByPriceRange() {
        double minPrice = 50.0;
        double maxPrice = 100.0;
        List<Product> matchingProducts = repository.searchProductsByPriceRange(minPrice, maxPrice);

        Assertions.assertFalse(matchingProducts.isEmpty());

        for (Product product : matchingProducts) {
            Assertions.assertTrue(product.getPrice() >= minPrice && product.getPrice() <= maxPrice);
        }
    }

    @Test
    public void testSearchProductsByQuantityRange() {
        int minQuantity = 5;
        int maxQuantity = 20;
        List<Product> matchingProducts = repository.searchProductsByQuantityRange(minQuantity, maxQuantity);

        Assertions.assertFalse(matchingProducts.isEmpty());

        for (Product product : matchingProducts) {
            Assertions.assertTrue(product.getQuantity() >= minQuantity && product.getQuantity() <= maxQuantity);
        }
    }
}
