package com.furniture.service;

import com.furniture.model.Product;
import com.furniture.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ProductServiceTest {
    private ProductRepository productRepository;
    private ProductService productService;

    @BeforeEach
    public void setup() {
        productRepository = Mockito.mock(ProductRepository.class);
        productService = new ProductService(productRepository);
    }

    @Test
    public void testGetAllProducts() {
        List<Product> expectedProducts = createProductList();
        when(productRepository.getAllProducts()).thenReturn(expectedProducts);

        List<Product> actualProducts = productService.getAllProducts();

        assertEquals(expectedProducts, actualProducts);
    }

    @Test
    public void testSearchProductsByName() {
        String name = "Chair";
        List<Product> expectedProducts = createProductList();
        when(productRepository.searchProductsByName(name)).thenReturn(expectedProducts);

        List<Product> actualProducts = productService.searchProductsByName(name);

        assertEquals(expectedProducts, actualProducts);
    }

    @Test
    public void testSearchProductsByCategory() {
        String category = "Furniture";
        List<Product> expectedProducts = createProductList();
        when(productRepository.searchProductsByCategory(category)).thenReturn(expectedProducts);

        List<Product> actualProducts = productService.searchProductsByCategory(category);

        assertEquals(expectedProducts, actualProducts);
    }

    @Test
    public void testSearchProductsByPriceRange() {
        double minPrice = 50.0;
        double maxPrice = 100.0;
        List<Product> expectedProducts = createProductList();
        when(productRepository.searchProductsByPriceRange(minPrice, maxPrice)).thenReturn(expectedProducts);

        List<Product> actualProducts = productService.searchProductsByPriceRange(minPrice, maxPrice);

        assertEquals(expectedProducts, actualProducts);
    }

    @Test
    public void testSearchProductsByQuantityRange() {
        int minQuantity = 5;
        int maxQuantity = 10;
        List<Product> expectedProducts = createProductList();
        when(productRepository.searchProductsByQuantityRange(minQuantity, maxQuantity)).thenReturn(expectedProducts);

        List<Product> actualProducts = productService.searchProductsByQuantityRange(minQuantity, maxQuantity);

        assertEquals(expectedProducts, actualProducts);
    }

    // Helper method to create a sample product list for testing
    private List<Product> createProductList() {
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, "Chair", "Furniture", 49.99, 10));
        products.add(new Product(2, "Bookcase", "Furniture", 79.99, 5));
        products.add(new Product(3, "Fluffy", "Decor", 29.99, 15));
        return products;
    }
}
