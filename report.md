| Table Progeress report |

| Tukaeva Izabella       | Stage   | Start date | End date | Comment |
|---------|---------|------------|----------|---------|
| 1. Task Clarification  | Recieve and analyze the task | 19 March 09:30     |   19 March 11:20 | Many misundesrtandinga  | 
| 2. Task Clarification  |  Clarify details and requirements with the mentor | 19 March 11:20     |   |  We haven't clarified the details yet | 
| 3. Task Clarification  |   Propose a 10-week work plan for course project development | 20 March 15:30     |   |   | 
| 4. Task Clarification  |  Create a progress report and place it in your repository | 20 March   17:00   |   |   | 
| --- | --- | --- | ---| --- |
| 1. Analysis  |  Study the applied area by the individual task |  21 March 09:00     |   |   | 
| 2. Analysis  |  Create a progress report and place it in your repository | 21 March 11:00   |   |   | 
| 3. Analysis  |  Define and describe the basic functionality that needs to be implemented first (MVP—minimum viable product) | 22 March 16:30     |   |   | 
| 4. Analysis  |  Describe additional functionality for improving usability, security, performance, etc. | 22 March 19:00     |   |   | 
| 5. Analysis  | Describe any advanced functionality that might be useful in the future | 23 March 10:00     |   |   | 
| --- | --- | --- | ---| --- |
| 1. Use Cases  | Describe several options for using the program (how the program should behave from the user's point of view) however you prefer. | 25 March 09:00     |   |   |